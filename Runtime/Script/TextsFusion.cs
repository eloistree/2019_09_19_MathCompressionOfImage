﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TextsFusion : MonoBehaviour
{
    public TextAsset [] m_texts;
    public string m_fileLocalLocationPath = "/../TextFusions.csv";
    [Header("Debug")]
    public string m_path;
    // Start is called before the first frame update
    void Start()
    {
        m_path = Application.dataPath + m_fileLocalLocationPath;
        if (!File.Exists(m_path))
            File.WriteAllText(m_path, "");
        for (int i = 0; i < m_texts.Length; i++)
        {
            File.AppendAllText(m_path,m_texts[i].text);

        }
    }
    
}
