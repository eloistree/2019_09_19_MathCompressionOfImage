﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public class ApBmDpC : MonoBehaviour
{
    [Range(2, 999)]
    public int number = 2;
    [Range(2, 11)]
    public int exponent = 2;
    [Range(1f, 2f)]
    public double multiplicator = 1.0001;
    [Range(0, 9999)]
    public int rest = 0;
    public string equation;
    public string result;
    [Header("Try to attempt")]
    public string toAttempt="0";
    public string toRestOfTry;

    private void OnValidate()
    {

        multiplicator = (Math.Truncate((double)multiplicator * 100000000.0) / 100000000.0);
        equation = string.Format("({0}^{1})*{2}+{3}", number, exponent, multiplicator, rest);
        BigInteger pow = BigInteger.Pow(new BigInteger(number), exponent);
        BigInteger multiplicatorAsInteger = new BigInteger(multiplicator * 100000000);
        BigInteger multipow = BigInteger.Multiply(pow, multiplicatorAsInteger);
        BigInteger powMultiButDividedAsInteger = BigInteger.Divide(multipow, 100000000);
        BigInteger restbig = new BigInteger(rest);
        result = (powMultiButDividedAsInteger + restbig).ToString();

        if (result == toAttempt) {
            toAttempt = BigIntegerToolbox.GetRandomAsString(UnityEngine.Random.Range(9, 18));
        }
        toRestOfTry = (BigInteger.Parse(toAttempt) - (powMultiButDividedAsInteger + restbig)).ToString();
    }

    private void Reset()
    {
        toAttempt = BigIntegerToolbox.GetRandomAsString(UnityEngine.Random.Range(9, 18));
    }
}
