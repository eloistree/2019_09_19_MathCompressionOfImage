﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

[HelpURL("https://www.mathsisfun.com/algebra/logarithms.html")]
public class Exp_LogToAB : MonoBehaviour
{
    //https://www.mathsisfun.com/algebra/logarithms.html
    public string m_bigNumber="27";
    public long m_logBase =3;

    [Header("Result")]
    public double m_logValue;
    [Header("Result")]
    public string m_bigNumberLowTier;
    public string m_bigNumberHighTier;
    public string m_bigNumberRest;
    public string m_bigNumberDecimal;


    [Header("0-9999")]
    public float m_timeBetween=1;
    public Solution m_bestSolution = new Solution() { m_number=2,m_exponent=2,m_decimalLeft=1f};
    public List<Solution> m_veryCloseSolution;


    public double ExtractDecimal(double value)
    {
        double numberWithoutDecimal = (int)value;
        return value - numberWithoutDecimal;

    }

    [System.Serializable]
    public class Solution
    {
        public string m_target;
        public string m_lowTier;
        public string m_rest;
        public long m_number;
        public int m_exponent;
        public double m_decimalLeft;
    }
    public long index = 2;
    public long indexMax = 65536;

    [Range( 0.01f, 1f)]
    public float pourcent;
    private float previousPourcent;
    public IEnumerator Start()
    {
        BigInteger p = BigInteger.Parse(m_bigNumber);

       while (index< indexMax && new BigInteger(index)<=p)
        {

            m_logBase = index;
            Compute();

            double decimalRest = ExtractDecimal(m_logValue);
            if (decimalRest <= m_bestSolution.m_decimalLeft) {
                SaveSolution(m_logBase, (int)m_logValue, decimalRest, m_bigNumber, m_bigNumberLowTier, m_bigNumberRest);
            }
            index++;
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(m_timeBetween);
        }
        
    }

    private void SaveSolution(long number, int exponent, double decimalRest, string bigNumber, string lowTier, string rest)
    {
        if(m_bestSolution.m_target.Length>0 && m_bestSolution.m_decimalLeft<0.0001)
           m_veryCloseSolution.Add(m_bestSolution);
        m_bestSolution = new Solution() { m_target = bigNumber, m_lowTier = lowTier, m_rest= rest,  m_number = number, m_exponent = exponent, m_decimalLeft = decimalRest };

    }

    public void OnValidate()
    {
        //Debug.Log("> " + (BigInteger.Pow(2307, 200) + new BigInteger(32450)));
        Compute();
        if (previousPourcent != pourcent) {
            previousPourcent = pourcent;
           index =(int) ( pourcent * (float)indexMax);
        }

    }

    private void Compute()
    {
        m_logValue = BigInteger.Log(BigInteger.Parse(m_bigNumber), m_logBase);
        m_bigNumberLowTier = BigInteger.Pow(new BigInteger(m_logBase), (int)(m_logValue)).ToString();
        m_bigNumberHighTier = BigInteger.Pow(new BigInteger(m_logBase+1), (int)(m_logValue )).ToString();
        m_bigNumberRest = (BigInteger.Parse(m_bigNumber) - BigInteger.Parse(m_bigNumberLowTier)).ToString();
        m_bigNumberDecimal = GetIntegerRestFromDecimalApproximation().ToString();
    }

    private BigInteger GetIntegerRestFromDecimalApproximation()
    {
        return new BigInteger(0);
    }
}
