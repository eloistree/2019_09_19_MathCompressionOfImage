﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using UnityEngine;

public class Exp_ShortCutBridge : MonoBehaviour
{
    public string m_localPath="ShortCutBridge_{0:0000}.csv";

    public int m_countPerFrame=4;


    public int m_maxNumber = 9999;
    public int m_maxExponent = 99;

    public int m_indexNumber;
    public int m_indexExponent;
    public int m_lenght;
    [TextArea(0,10)]
    public string m_result;


    IEnumerator Start()
    {
        Debug.Log("Start");
        for (int i = 0; i < 300; i++)
        {
            File.WriteAllText(GetPath(i) , "");

        }
      
            for (int j = 2; j < m_maxExponent; j++)
            {
                m_indexExponent = j;
                for (int i = 0; i < m_maxNumber; i++)
                {
                    m_indexNumber = i;
                    if(i%m_countPerFrame==0)
                      yield return new WaitForEndOfFrame();
                  
                    m_result = BigInteger.Pow(i, j).ToString();
                    m_lenght = m_result.Length;
                    if (m_result.Length<300)
                        File.AppendAllText(GetPath(m_result.Length), string.Format("{0},{1},{2}\n", i, j, m_result));
                }
            }

        Debug.Log("Stop");
        yield break;
        
        
    }
    
    private string GetPath(int lenght)
    {
        return Application.dataPath + "/" + string.Format(m_localPath, lenght);
    }
}
