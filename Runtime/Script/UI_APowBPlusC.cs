﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class UI_APowBPlusC : MonoBehaviour
{
    public InputField m_number;
    public InputField m_exponent;
    public InputField m_rest;

    [Header("Debug")]
    public InputField m_result;
    public Text m_resultLenght;
    public Text m_formulaLenght;

    void Start()
    {
        m_number.onValueChanged.AddListener(Refresh);
        m_exponent.onValueChanged.AddListener(Refresh);
        m_rest.onValueChanged.AddListener(Refresh);
    }

    private void Refresh(string arg0)
    {

        try
        {
            BigInteger resultCompted;
            CompressionApBpC.Compute(m_number.text, m_exponent.text, m_rest.text, out resultCompted);
            m_result.text = resultCompted.ToString();
            m_resultLenght.text =""+ m_result.text.Length;
            m_formulaLenght.text =""+ (2+m_number.text.Length + m_exponent.text.Length + m_rest.text.Length );
        }
        catch (Exception e) {
            m_result.text = "0";
        }
  
    }
    
}
