﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class ComputeBridgeBetween9999Pow99 : MonoBehaviour
{

    public string m_absoluteFilePathWith9999Pow99 = "";
    public string m_fileLocalLocationPath = "/../9999Pow99Bridge.csv";
    [Header("Debug")]
    public string m_path;

    public ABResult m_previous = new ABResult("1","1","1");
    public ABResult m_current= new ABResult("1","1","1");

    public ulong m_count;
    public string m_currentLine;
    public string m_lineCountAsString;
    public BigInteger m_lineCount = new BigInteger(0);

    public string m_lastDifference;
    public int waitEveryFrames = 100;
    public float m_timeBetween = 0.1f;
    


    IEnumerator Start()
    {
      
        yield return ComputeBridgeBetweenNumber();
    }
    
    private IEnumerator ComputeBridgeBetweenNumber()
    {
        // convert string to stream
        m_path = Application.dataPath + m_fileLocalLocationPath;
        if (!File.Exists(m_path))
            File.WriteAllText(m_path, "");
        
      
            using (StreamReader sr = new StreamReader(m_absoluteFilePathWith9999Pow99))
            {
                while (sr.Peek() >= 0)
                {
                    m_currentLine = sr.ReadLine();
                    string[] tokens = m_currentLine.Split(',');
                    if (tokens.Length == 3 )
                    {
                         ABResult ab = new ABResult(tokens[0], tokens[1], tokens[2]);
                        m_previous = m_current;
                        m_current = ab;
                        BigInteger difference = BigInteger.Parse(m_current.m_result) - BigInteger.Parse(m_previous.m_result);
                        m_lastDifference = difference.ToString();
                        File.AppendAllText(m_path, string.Format("{0},{1},{2}, {3}\n", m_previous.m_number, m_previous.m_exponent, m_previous.m_result, difference.ToString()));
                    }

                    if (m_lineCount % waitEveryFrames == 0) {

                        m_lineCountAsString.ToString();
                        yield return new WaitForSeconds(m_timeBetween);
                    }
                    m_lineCount++;
                    m_lineCountAsString.ToString();
                 }
            
            yield return new WaitForSeconds(m_timeBetween);

            }
    }

    public class ABResult
    {
        public string m_number="1";
        public string m_exponent="1";
        public string m_result="1";

        public ABResult(string number, string exponent, string result)
        {
            this.m_number = number;
            this.m_exponent = exponent;
            this.m_result = result;
        }
    }
}
