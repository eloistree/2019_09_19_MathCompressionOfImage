﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exp_Feel200Digit : MonoBehaviour
{

    [TextArea(0,10)]
    public string m_number_2;
    [TextArea(0, 10)]
    public string m_number_10;
    [TextArea(0, 10)]
    public string m_number_20;
    [TextArea(0, 10)]
    public string m_number_50;
    [TextArea(0, 10)]
    public string m_number_100;
    [TextArea(0, 10)]
    public string m_number_150;
    [TextArea(0, 10)]
    public string m_number_200;

    [TextArea(0, 10)]
    public string m_number_500;

    [TextArea(0, 10)]
    public string m_number_1000;
    [TextArea(0, 10)]
    public string m_number_2000;
    [TextArea(0, 10)]
    public string m_number_5000;


    private void Reset()
    {
        m_number_2 = BigIntegerToolbox.GetRandomAsString(2);
        m_number_10 = BigIntegerToolbox.GetRandomAsString(10);
        m_number_20 = BigIntegerToolbox.GetRandomAsString(20);
        m_number_50 = BigIntegerToolbox.GetRandomAsString(50);
        m_number_100 = BigIntegerToolbox.GetRandomAsString(100);
        m_number_150 = BigIntegerToolbox.GetRandomAsString(150);
        m_number_200 = BigIntegerToolbox.GetRandomAsString(200);
        m_number_500 = BigIntegerToolbox.GetRandomAsString(500);
        m_number_1000 = BigIntegerToolbox.GetRandomAsString(1000);
        m_number_2000 = BigIntegerToolbox.GetRandomAsString(2000);
        m_number_5000 = BigIntegerToolbox.GetRandomAsString(5000);

    }
    
}
