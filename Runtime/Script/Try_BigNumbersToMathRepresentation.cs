﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class Try_BigNumbersToMathRepresentation : MonoBehaviour
{

    public int m_miliSeconds;
    public LookForBestSolution m_currentBestSolution;
    public bool m_valideChange;

    [Range(0, 100000000)]
    public long m_aBig;
    [Range(0, 100000)]
    public long m_aMedium; 
     [Range(-1000, 1000)]
    public long m_a1000;
    [Range(-100, 100)]
    public long m_a100;
    [Range(-10, 10)]
    public long m_a10;
    [Range(1,2000)]
    public long m_b;
    [TextArea(0, 20)]
    public string m_dust;

    [TextArea(0, 20)]
    public string m_tryToApproach;
    [TextArea(0, 20)]
    public string m_bigNumber;


    [TextArea(0, 20)]
    public string m_sqrt;


    public DateTime m_startProcessing;
    public DateTime m_endProcessing;

    [System.Serializable]
    public class LookForBestSolution
    {
        public long  bestSolution = long.MaxValue;
        public long a;
        public long b;
        [TextArea(0, 20)]
        public string dust;
    }

    public IEnumerator Start()
    {
        //m_aBig = 1;
        //m_b = 1;


        while (true) {
            for (long i = m_b; i < 200; i++)
            {
                m_b = i;
                for (long j = m_aBig; j < 1000; j++)
                {
                    m_aBig = j * 1000;
                   yield return new WaitForEndOfFrame();
                //ComputeDust();
                if (m_dust.Length < m_currentBestSolution.bestSolution)
                {
                    m_currentBestSolution.bestSolution = m_dust.Length;
                    m_currentBestSolution.a = m_aBig + m_aMedium + m_a1000 + m_a100 + m_a10;
                    m_currentBestSolution.b = m_b;
                    m_currentBestSolution.dust = m_dust;
                }

                }
            }
        }
    }

    private void OnValidate()
    {
        if (m_valideChange)
        {
          //  m_valideChange = false;
            ComputeDust();
            
            m_sqrt =""+ Math.Pow(Math.E, BigInteger.Log(BigInteger.Parse(m_bigNumber)) / 2);
        }

    }

    private void ComputeDust()
    {
        m_startProcessing = DateTime.Now;
        BigInteger number = BigInteger.Parse(m_bigNumber);
        BigInteger tryToApproche = BigPow(GetApproximationByPow(), new BigInteger(m_b));
        BigInteger dust = number - tryToApproche;
        m_tryToApproach = tryToApproche.ToString();
        m_dust = dust.ToString();

        m_endProcessing = DateTime.Now;
        TimeSpan duration = (m_endProcessing - m_startProcessing);
        m_miliSeconds = duration.Seconds * 1000 + duration.Milliseconds;
    }

    private BigInteger GetApproximationByPow()
    {
       return  new BigInteger(m_aBig) + new BigInteger(m_aMedium) + new BigInteger(m_a1000) + new BigInteger(m_a100) + new BigInteger(m_a10);
    }

    private BigInteger BigPow(BigInteger value, BigInteger index)
    {
        BigInteger result = 1;
        for (int i = 0; i < index; i++)
        {

            result *= value;
        }
        return result;

    }
    private BigInteger BigInersePow(BigInteger value, BigInteger index)
    {
        throw new System.NotImplementedException();
        //BigInteger result = 1;
        //for (int i = 0; i < index; i++)
        //{

        //    result *= BigInteger.Divide(new BigInteger(1) /);
          
        //}
        //return result;

    }
}
