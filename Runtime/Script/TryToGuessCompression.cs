﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public class TryToGuessCompression : MonoBehaviour
{
     [TextArea(0,6)]
    public string m_bigNumberToGuess;
    public long m_bestGuessIteration;
    public int m_maxNumber=5;
    public int m_maxExponent = 5;
    public float m_timeBetweenGuess=0.1f;
    public int m_interationPerFrame = 10;
    [Header("Debug")]
    [Range(0, long.MaxValue)]
    public long m_iteration;
    public CompressionApBpC m_bestGuess;
    public CompressionApBpC m_lastGuess ;

   

    IEnumerator Start()
    {
        BigInteger toGuess = BigInteger.Parse(m_bigNumberToGuess);
        while (true) {
           yield return new WaitForEndOfFrame();
           yield return new WaitForSeconds(m_timeBetweenGuess);
            for (int l = 0; l < m_interationPerFrame; l++)
            {

                string a ="";
                for (int i = 0; i < UnityEngine.Random.Range(1, m_maxNumber); i++)
                {
                    a += "" + UnityEngine.Random.Range(0, 9);
                }


                string b="";
                for (int i = 0; i < UnityEngine.Random.Range(1, m_maxExponent); i++)
                {
                    b += "" + UnityEngine.Random.Range(0, 9);
                }
                BigInteger result = BigInteger.Pow(BigInteger.Parse(a), int.Parse(b));
                BigInteger rest = toGuess - result;
                CompressionApBpC compression = new CompressionApBpC();
                compression.SetWith(a, b, result.ToString(), rest.ToString());  
                m_lastGuess = compression;
                if (m_lastGuess.GetLenght() < m_bestGuess.GetLenght()) {
                    m_bestGuessIteration = m_iteration;
                    m_bestGuess = m_lastGuess;
                    Debug.Log("New record: " + m_bestGuess.GetLenght());
                }
                m_iteration++;
            }
        }
    }
    
}
[System.Serializable]
public class CompressionApBpC
{
    public void SetWith(string number, string exponent, string result, string rest)
    {
        m_number = number;
        m_exponent = exponent;
        m_result = result;
        m_rest = rest;
    }

    [TextArea(0, 6)]
    public string m_number;
    [TextArea(0, 6)]
    public string m_exponent;
    [TextArea(0, 6)]
    public string m_result;
    [TextArea(0, 6)]
    public string m_rest;

    public int GetLenght() { return m_rest.Length; }
    public static void Compute(string number, string exponent, string rest, out CompressionApBpC result)
    {
        result = new CompressionApBpC();
        BigInteger resultComputed;
        Compute(BigInteger.Parse(number), int.Parse(exponent), BigInteger.Parse(rest), out resultComputed);
        result.SetWith(number, exponent, resultComputed.ToString(), rest);
    }
    public static void Compute(string number, string exponent, string rest, out BigInteger result)
    {
        Compute(BigInteger.Parse(number), int.Parse(exponent), BigInteger.Parse(rest), out result);
    }
    public static void Compute(BigInteger number, int exponent, BigInteger rest, out BigInteger result)
    {
        result = BigInteger.Pow(number, exponent) + rest;
    }
}


public class BigIntegerToolbox {

    /// <summary>
    /// NOT CHECKED YET
    /// </summary>
    /// <param name="bigNumber"></param>
    /// <returns></returns>
    public static double GetSqrtAsDouble(string bigNumber)
    {
        return GetSqrtAsDouble(BigInteger.Parse(bigNumber));
    }
    /// <summary>
    /// NOT CHECKED YET
    /// </summary>
    /// <param name="bigNumber"></param>
    /// <returns></returns>
    public static double GetSqrtAsDouble(BigInteger bigNumber) {
       return Math.Pow(Math.E, BigInteger.Log(bigNumber) / 2);
    }

    public static BigInteger BigPow(BigInteger value, BigInteger index)
    {
        BigInteger result = 1;
        for (int i = 0; i < index; i++)
        {

            result *= value;
        }
        return result;

    }
    public static object Pow(BigInteger bigNumber, double exponent)
    {
        throw new NotImplementedException();
    }

    public static double DivideByLog(BigInteger value, BigInteger divider) {
        //https://stackoverflow.com/questions/11859111/biginteger-division-in-c-sharp
        return Math.Exp(BigInteger.Log(value) - BigInteger.Log(divider));
    }


    public static string GetRandomAsString(int length)
    {
        string result = "";
        for (int i = 0; i < length; i++)
        {
            result += UnityEngine.Random.Range(0, 10);
        }
        return result;
    }
    public static BigInteger GetRandom(int length)
    {
        return BigInteger.Parse(GetRandomAsString(length));
    }

   
}