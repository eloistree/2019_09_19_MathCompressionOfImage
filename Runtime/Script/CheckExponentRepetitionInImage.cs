﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class CheckExponentRepetitionInImage : MonoBehaviour
{
    public TextAsset m_imageAsBigInteger;
    public TextAsset [] m_abCollection;
    public List<APowB> m_apowb;
    private string m_bigInteger;
    public long m_startLenght;
    public long m_currentLenght;
    public int m_index;
    IEnumerator Start()
    {
        Debug.Log("Load Big Integer Collecting");
        m_bigInteger = m_imageAsBigInteger.text;
        Debug.Log("Big Ingteger loaded Collecting");
        m_startLenght = m_bigInteger.Length;

        for (int j = 0; j < m_abCollection.Length; j++)
        {

            Debug.Log("Next Collections: " + m_abCollection[j].name);
            yield return new WaitForSeconds(1f);


            Debug.Log("Start Collecting");
             m_apowb = APowBCollection.GetAPowBFromCSV(m_abCollection[j].text);
              Debug.Log("Stop Collecting");

            for (int i = 0; i < m_apowb.Count; i++)
            {
                //m_bigInteger =m_bigInteger.Replace(m_apowb[i].m_result, "#");
                MatchCollection col = Regex.Matches(m_bigInteger, m_apowb[i].m_result);
                if (col.Count > 0)
                {
                    yield return new WaitForSeconds(0.5f);
                    Debug.Log(string.Format("Count ({0}): {1}^{2} {3} | {4}", col.Count, m_apowb[i].m_number, m_apowb[i].m_exponent, m_apowb[i].m_result.Length, m_apowb[i].m_result));
                }
                m_index = i;
                yield return new WaitForEndOfFrame(); 
            }
            m_currentLenght = m_bigInteger.Length;
        }
    }
    
}
public class APowB
{
    public string m_number;
    public string m_exponent;
    public string m_result;

    public APowB(string number, string exponent, string result)
    {
        this.m_number = number;
        this.m_exponent = exponent;
        this.m_result = result;
    }
}

public class APowBCollection {

  


    public static  List<APowB> GetAPowBFromCSV(string text)
    {

        byte[] byteArray = Encoding.ASCII.GetBytes(text);
        MemoryStream stream = new MemoryStream(byteArray);
        return GetAPowBFromCSV(stream);
    }

        public static  List<APowB> GetAPowBFromCSV(Stream stream)
    {
        List<APowB> result = new List<APowB>();

        Dictionary<string, string> alreadyIn = new Dictionary<string, string>();
        using (StreamReader sr = new StreamReader(stream))
        {
            while (sr.Peek() >= 0)
            {
                string line = sr.ReadLine();
                string[] tokens = line.Split(',');
                if (tokens.Length == 3 && int.Parse(tokens[1]) > 1)
                {
                    string id = tokens[0] + "x" + tokens[1];
                    if (!alreadyIn.ContainsKey(id))
                    {
                        alreadyIn.Add(id, "");
                        result.Add(new APowB(tokens[0], tokens[1], tokens[2]));
                      
                    }
                }
            }
        }
        return result;
    }
    //public string  GetAPowBFromCSV(List<APowB> collection)
    //{

    //}

}