﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public class Try_ShortcutBridge : MonoBehaviour
{
    public string m_range = "99999999";
    [Range(1,2501)]
    public int m_numberLenght = 20;
    [TextArea(0,20)]
    public string m_number = "";
    public string m_bridgeRequired;



    private void OnValidate()
    {

        m_number = BigIntegerToolbox.GetRandom(m_numberLenght).ToString();
        m_bridgeRequired = BigInteger.Divide(BigInteger.Parse( m_number), BigInteger.Parse(m_range)).ToString();
    }
    private void Reset()
    {
        m_number = BigIntegerToolbox.GetRandom(m_numberLenght).ToString() ;
    }
}
