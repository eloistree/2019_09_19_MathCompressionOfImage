﻿
using System.Numerics;

public class RandombigIntegerDefaultMono : RandomBigIntegerMono
{
    public int m_minLenght = 10;
    public int m_maxLenght = 20;
    public override BigInteger GetRandomInteger()
    {
        return BigIntegerToolbox.GetRandom(UnityEngine.Random.Range(m_minLenght, m_maxLenght));

    }
    private void OnValidate()
    {
        if (m_maxLenght < m_minLenght)
        {
            int d = m_minLenght;
            m_minLenght = m_maxLenght;
            m_maxLenght = d;
        }
    }
}

