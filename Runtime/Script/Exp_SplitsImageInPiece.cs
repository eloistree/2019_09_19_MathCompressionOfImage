﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class Exp_SplitsImageInPiece : MonoBehaviour
{
    public InputField m_width;
    public InputField m_height;
    public InputField m_multiplicator;
    public InputField m_numberPixels;
    public InputField m_splitSize;
    public InputField m_splitCount;
    // Start is called before the first frame update
    void Start()
    {
        m_width.onValueChanged.AddListener(RefreshPixelCount);
        m_height.onValueChanged.AddListener(RefreshPixelCount);
        m_multiplicator.onValueChanged.AddListener(RefreshPixelCount);
        m_width.onValueChanged.AddListener(RefreshSplit);
        m_height.onValueChanged.AddListener(RefreshSplit);
        m_multiplicator.onValueChanged.AddListener(RefreshSplit);
        m_splitSize.onValueChanged.AddListener(RefreshSplit);
        RefreshPixelCount("");
        RefreshSplit("");
    }

    private void RefreshSplit(string arg0)
    {
        try
        {
            m_splitCount.text = (BigInteger.Parse(m_numberPixels.text) / BigInteger.Parse(m_splitSize.text)).ToString();
        }
        catch (Exception) { }
    }

    private void RefreshPixelCount(string arg0)
    {
        try
        {
            m_numberPixels.text = (BigInteger.Parse(m_width.text) * BigInteger.Parse(m_height.text) * BigInteger.Parse(m_multiplicator.text)).ToString();
        }
            catch (Exception) { }
    }
    
}
