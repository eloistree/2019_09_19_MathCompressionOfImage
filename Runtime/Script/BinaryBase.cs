﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BinaryBase : MonoBehaviour
{
    public long m_base = 999;
    [TextArea(0, 20)]
    public string m_maxBaseBinary;
    [TextArea(0,20)]
    public string m_text;

    public char m_character;
    public byte [] m_charByte;
    public string m_charByteString;

    public string m_format = "{0:0000000}:{1} {2} byte(s)\n";
    IEnumerator Start()
    {
        
        m_text = "";

        for (int i = 0; i < m_base; i++)
        {
            yield return new WaitForEndOfFrame();
            AddLineWith(i);
        }
    }

    private void AddLineWith(int i)
    {
        string s = Convert.ToString(i, 2);
        m_text += string.Format(m_format, i, s ,(1 + s.Length / 8));
    }

    private void OnValidate()
    {
        string s = Convert.ToString(m_base, 2);
        m_maxBaseBinary = s + " " + (1 + s.Length / 8) + "byte(s) \n";

        byte[] m_charByte = Encoding.ASCII.GetBytes("" + m_character);
        m_charByteString = ByteArrayToString(m_charByte);

    }
    public static string ByteArrayToString(byte[] ba)
    {
        return string.Join(" ",    ba.Select(x => Convert.ToString(x, 2).PadLeft(8, '0')));
    }
}
