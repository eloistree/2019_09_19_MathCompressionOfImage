﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_NumberRangeSlider : MonoBehaviour
{
    public double m_value;
    public bool m_cropToInteger=true;
    public InputField m_maxValue;
    public Slider m_pourcentSlider;
    public NumberRangeChange m_onChange;
    public NumberRangeChangeAsString m_onChangeAsText;

    void Start()
    {
        m_maxValue.onValueChanged.AddListener(Compute);
        m_pourcentSlider.onValueChanged.AddListener(Compute);

    }

    private void Compute(float arg0)
    {
        Refresh();
    }

    private void Compute(string arg0)
    {
        Refresh();
    }
    
    void Refresh()
    {
        m_value = double.Parse(m_maxValue.text) * ((double) m_pourcentSlider.value);
        if (m_cropToInteger)
            m_value = (int)m_value;
        m_onChange.Invoke(m_value);
        m_onChangeAsText.Invoke("" + m_value);
    }

}
[System.Serializable]
public class NumberRangeChange : UnityEvent<double> { }
[System.Serializable]
public class NumberRangeChangeAsString : UnityEvent<string> { }
