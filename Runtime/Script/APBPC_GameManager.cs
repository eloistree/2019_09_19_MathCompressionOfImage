﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class APBPC_GameManager : MonoBehaviour
{
    public int m_level = 2;
    public InputField m_gameProposition;
    public Text m_displayLevel;
    public Text m_displayLenght;



    public void AnswerProposition(string answerAsText)
    {

        if (m_gameProposition.text == answerAsText) {
            m_level++;
            ProposeNewSolution();
        }
        
    }

    private void ProposeNewSolution()
    {
        m_gameProposition.text = GetRandomNumber(m_level);
        m_displayLevel.text = "" + m_gameProposition.text.Length;
        m_displayLenght.text = "" + m_gameProposition.text.Length;
    }

    public string GetRandomNumber(int count) {

        if (count <= 0) return "0";
        string result="";
        for (int i = 0; i < count; i++)
        {
            result += "" + UnityEngine.Random.Range(0, 9);

        }
        return result;

    }
}
