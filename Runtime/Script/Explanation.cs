﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explanation : MonoBehaviour
{
    [TextArea(2, 3)]
    public string m_title;
    [TextArea(4, 10)]
    public string m_description;
    
}
