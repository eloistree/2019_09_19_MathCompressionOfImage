﻿using System.Numerics;
using UnityEngine;

public abstract class RandomBigIntegerMono : MonoBehaviour
{

    public abstract BigInteger GetRandomInteger();
}
