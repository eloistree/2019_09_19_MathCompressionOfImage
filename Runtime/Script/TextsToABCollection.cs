﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public class TextsToABCollection : MonoBehaviour
{

    public TextAsset [] m_texts;
    public List<ABResult> m_results = new List<ABResult>();
    public long m_count;
    public string m_currentFile;
    public string m_currentLine;
    public int m_lineCount;

    public float m_timeBetween = 0.1f;
    [TextArea(0,20)]
     string m_ordered;

    public struct ABResult {
        public string m_number;
        public string m_exponent;
        public string m_result;

        public ABResult(string number, string exponent, string result)
        {
            this.m_number = number;
            this.m_exponent = exponent;
            this.m_result = result;
        }
    }
    IEnumerator Start()
    {
        yield return ImportABResult();  
    }

    public Dictionary<string, string> alreadyIn = new Dictionary<string, string>();
    private IEnumerator ImportABResult()
    {
        // convert string to stream
        

        for (int i = 0; i < m_texts.Length; i++)
        {
            m_ordered = "";
            m_currentFile = m_texts[i].name +" (start)";
            string txt = m_texts[i].text;

            byte[] byteArray = Encoding.ASCII.GetBytes(txt);
            MemoryStream stream = new MemoryStream(byteArray);

            using (StreamReader sr = new StreamReader(stream))
            {
                while (sr.Peek() >= 0)
                {
                    m_currentLine = sr.ReadLine();
                    string[] tokens = m_currentLine.Split(',');
                    if (tokens.Length == 3 && int.Parse(tokens[1]) > 1)
                    {
                        string id = tokens[0] + "x" + tokens[1];
                        if (!alreadyIn.ContainsKey(id))
                        {
                            alreadyIn.Add(id, "");
                            m_results.Add(new ABResult(tokens[0], tokens[1], tokens[2]));
                            m_count++;
                        }
                    }
                    if(m_lineCount%1000==0)
                        yield return new WaitForEndOfFrame();
                    m_lineCount++;
                }
            }


            m_results = m_results.OrderBy(k => k.m_result).ToList();

            for (int x = 0; x < m_results.Count; x++)
            {
                m_ordered += string.Format("{0},{1},{2}\n", m_results[x].m_number, m_results[x].m_exponent, m_results[x].m_result);
            }
            string path = Application.dataPath + "/../" + m_texts[i].name + "_UniqueOrdered.csv";
            File.WriteAllText(path, m_ordered);
            alreadyIn.Clear();
            m_results.Clear();
            m_currentFile = m_texts[i].name + " (End)";

            yield return new WaitForSeconds(m_timeBetween);

        }
    }

}
