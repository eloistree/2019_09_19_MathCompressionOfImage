﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_NumberRangeSliderGroup : MonoBehaviour
{

    public double m_value;
    public bool m_cropToInteger = true;
    public UI_NumberRangeSlider[] m_slider;
    public NumberRangeChange m_onChange;
    public NumberRangeChangeAsString m_onChangeAsText;
    // Start is called before the first frame update
    void Refresh()
    {
        m_value = 0;
        for (int i = 0; i < m_slider.Length; i++)
        {
            m_value += m_slider[i].m_value;
        }
        if (m_cropToInteger)
            m_value = (int)m_value;
        m_onChange.Invoke(m_value);
        m_onChangeAsText.Invoke("" + m_value);
    }

    // Update is called once per frame
    void Start()
    {
        for (int i = 0; i < m_slider.Length; i++)
        {
            m_slider[i].m_onChange.AddListener(Refresh);
        }
    }

    private void Refresh(double arg0)
    {
        Refresh();
    }
}
