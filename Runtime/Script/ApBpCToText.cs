﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;
using UnityEngine;

public class ApBpCToText : MonoBehaviour
{
    public long m_minNumber =30000;
    public long m_maxNumber = 32000;
    public int m_minExponent = 2;
    public int m_maxExponent = 20;
    public string m_filePathInEditor;
    public int m_countPerFrame=42;
    [Header("Debug")]
    public string m_number;
    public string m_exponent;
    [TextArea(1,8)]
    public string m_tableContent;
    public DateTime m_startCountingTime;
    public DateTime m_endCountingTime;
    public long m_timeInMilliSecond;

    IEnumerator Start()
    {
        if (!File.Exists(m_filePathInEditor))
            File.Create(m_filePathInEditor);

        long index=0;
        BigInteger tmp;
        string lastBigInteger = "";
        string formulaLenght = "";
        for (long exponentIndex = m_minExponent; exponentIndex <= m_maxExponent; exponentIndex++)
        {
            for (long numberIndex = m_minNumber; numberIndex <= m_maxNumber; numberIndex++)
            {
                if(index%m_countPerFrame==0)
                  yield return new WaitForEndOfFrame();
                m_startCountingTime = DateTime.Now;
                CompressionApBpC.Compute("" + numberIndex, "" + exponentIndex, "0", out tmp);
                 
                lastBigInteger = tmp.ToString();
                formulaLenght = numberIndex + "" + exponentIndex;
                m_endCountingTime = DateTime.Now;
                m_timeInMilliSecond = (int)(m_endCountingTime - m_startCountingTime).TotalMilliseconds;
                m_tableContent = string.Format("{3},({4}|{5})| {6}ms:{0}^{1}={2}\n", numberIndex, exponentIndex, lastBigInteger, index++, formulaLenght.Length, lastBigInteger.Length, m_timeInMilliSecond) + m_tableContent;
            }
        }

        SaveAsFile();
    }

    private void SaveAsFile()
    {
        File.WriteAllText(m_filePathInEditor, m_tableContent);
    }

    private void OnDestroy()
    {

        SaveAsFile();
        Application.OpenURL(m_filePathInEditor);
    }

    private void Reset()
    {
        m_filePathInEditor = Application.dataPath + "/../ExponentTable.txt";
    }
}
