﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//https://www.programiz.com/csharp-programming/variables-primitive-data-types
public class MaxLenghtCSharp : MonoBehaviour
{
    public byte m_byte= byte.MaxValue;
    public short m_short= short.MaxValue;
    public int m_integer=int.MaxValue;
    public long m_long= long.MaxValue;

    public sbyte m_sbyte = sbyte.MaxValue;
    public ushort m_ushort = ushort.MaxValue;
    public uint m_uint= uint.MaxValue;
    public ulong m_slong = ulong.MaxValue;

    public float m_float = float.MaxValue;
    public double m_double = double.MaxValue;
    public decimal m_decimal = decimal.MaxValue;
    [TextArea(0,30)]
    public string m_copyPast;

    private void Reset()
    {
        m_copyPast = "";
        m_copyPast += "\nByte:\t "+ m_byte;
        m_copyPast += "\nShort:\t " + m_short;
        m_copyPast += "\nInteger:\t " + m_integer;
        m_copyPast += "\nLong:\t " + m_long;
        m_copyPast += "\nSByte:\t " + m_sbyte;
        m_copyPast += "\nUShort:\t " + m_ushort;
        m_copyPast += "\nUInt:\t " + m_uint;
        m_copyPast += "\nSLong:\t " + m_slong;
        m_copyPast += "\nFloat:\t " + m_float;
        m_copyPast += "\nDouble:\t " + m_double;
        m_copyPast += "\nDecimal:\t " + m_decimal;
    }
}
