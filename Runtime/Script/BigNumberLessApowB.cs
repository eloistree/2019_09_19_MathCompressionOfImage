﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public class BigNumberLessApowB : MonoBehaviour
{

    [System.Serializable]
    public class SerializedAPowB     {
        [Range(0,9999)]
        public int m_number;
        [Range(0, 100)]
        public int m_exponent;
    }

    public int m_chinaisLenght;
    [TextArea(0, 20)]
    public string m_bigInteger;
    [TextArea(0, 20)]
    public string m_rest;
    public SerializedAPowB[] soustractor;

    private void OnValidate()
    {
        BigInteger number = BigInteger.Parse(m_bigInteger);
        for (int i = 0; i < soustractor.Length; i++)
        {
            try
            {
                number -= BigInteger.Pow(new BigInteger(soustractor[i].m_number), soustractor[i].m_exponent);
            }
            catch (Exception e) { }

        }
        m_rest = number.ToString();
    }
    private void Reset()
    {
        m_bigInteger = BigIntegerToolbox.GetRandomAsString(200);
        m_chinaisLenght = (int) ((float) m_bigInteger.Length / 4f);
    }
}
