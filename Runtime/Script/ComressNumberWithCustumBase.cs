﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public class ComressNumberWithCustumBase : MonoBehaviour
{

    [TextArea(0,20)]
    public string m_number = "184467440737095516168";
    public double  m_base = 4294967296.0;
    public double m_result =0;
    private double t;

    [TextArea(0, 20)]
    public string m_test ;
    [TextArea(0, 20)]
    public string m_rest;

    private void Start()
    {
        m_result = BigInteger.Log(BigInteger.Parse(m_number), m_base);
        int exponent = (int)m_result;
        t = m_result - 0.000000000000001; //((int)(m_result * 1000000000000000.0)) / 1000000000000000.0;
    
        m_test = new BigInteger(System.Math.Pow(m_base, t)).ToString(); //BigIntegerToolbox.Pow(new BigInteger(m_double), m_result) .ToString();
         m_rest = (BigInteger.Parse(m_number) - BigInteger.Parse(m_test)).ToString();
    }
}
