﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Try_TextureToString : MonoBehaviour
{
    public RenderTexture m_target;
    public Texture2D m_current;
    public int m_pixelsCount;
    Color[] m_colors;
    string [] m_colorsAsNumber;
    string m_text;
   // public InputField m_textureAsText;
    public Text m_numberOfChars;
    public string m_textFormatPath;

    public void ComputeAndSave()
    {
        m_current = toTexture2D(m_target);
        m_colors = m_current.GetPixels();
        m_pixelsCount = m_colors.Length;
        m_colorsAsNumber = m_colors.Select(k => string.Format("{0:000}{1:000}{2:000}", Mathf.Round(k.r * 255f), Mathf.Round(k.g * 255f), Mathf.Round(k.b * 255f))).ToArray<string>();
        m_text = string.Join("|", m_colorsAsNumber);
        m_numberOfChars.text = ""+ m_text.Length;
        if( m_text.Length>0)    
        File.WriteAllText(m_textFormatPath, m_text);
    }

    Texture2D toTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(rTex.width, rTex.height, TextureFormat.RGB24, false);
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }
    private void Reset()
    {
        m_textFormatPath = Application.dataPath+"/../ImagesAsColorsText.txt";
    }
}
